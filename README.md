# Spring AMQP with RabbitMQ, part 4 #

## Request/Response pattern using Direct Reply-TO ##

Before you continue reading I highly recommend to read overview of my previous repo.

```
https://bitbucket.org/tomask79/spring-rabbitmq-request-response
```

Fixed queue for synchronous request/response pattern was the only reasonable option 
before rabbitmq version 3.4.0. After 3.4.0 *fixed queue is not needed anymore and direct 
reply-to mechanism is highly recommended, **because you don't need to configure any reply 
rabbitmq artifacts and rabbitmq will route the reply directly to waiting channel**!*
Let's test it! 

## Direct reply-to in action ##

For the demo of direct reply to let's modify previous demo of request/response pattern from
the mentioned link. *Changes in rabbitmq artifacts: **"We will completely remove the reply infrastructure"** !*

Direct reply-to rabbitmq config:

* **Exchange**: test_exchange
* **Routing Key**: greeting 
* **Request queue**: greeting, binded to test_exchange

### Configuration of Spring Beans ###

Direct reply-to is much more easier to configure compare to request/response with fixed queue. Let's take a look:


```
    @Bean
	public Queue greeting() {
		return new Queue("greeting");
	}
	
    MessageListener receiver() {
        return new MessageListenerAdapter(new RabbitMqReceiver(), "onMessage");
    }
    
    @Bean
	public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory, Queue replies) {
		RabbitTemplate template = new RabbitTemplate(connectionFactory);
		template.setExchange("test_exchange");
		template.setRoutingKey("greeting");		
		template.setReplyTimeout(60000);
		return template;
	}
	
	@Bean
	public SimpleMessageListenerContainer serviceListenerContainer(
			ConnectionFactory connectionFactory) {
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
		container.setConnectionFactory(connectionFactory);
		container.setQueues(greeting());
		container.setMessageListener(receiver());
		return container;
	}
	
	@Bean
	public RabbitMqSender sender() {
		final RabbitMqSender sender = new RabbitMqSender();
		return sender;
	}
```
Notice, that **we didn't configure any reply queue**! We're just having RabbitTemplate for sending messages into exchange
and receiver which waits for that messages via SimpleMessageListenerContainer and replies via RabbitMqReceiver POJO and that's all!
Sending code remains the same:


```
/**
 * @author Tomas Kloucek
 */
public class RabbitMqSender {

	@Autowired
	private RabbitTemplate template;
	
	public void send() {
		Message message = MessageBuilder.withBody("TomasKloucek".getBytes())
				.setContentType("text/plain")
				.build();

		// greeting
		Message reply = this.template.sendAndReceive(message);
		System.out.println("Reply from server is: "+new String(reply.getBody()));
	}
}
```

### Running the demo ###

* git clone this repo
* mvn clean install
* java -jar target/demo-0.0.1-SNAPSHOT.jar

after last command you should see the following output (of course set IP of your rabbitmq in connectionFactory bean!..:-)


```
Creating receiving configuration.
2016-05-19 22:49:42.173  INFO 6452 --- [           main] o.s.j.e.a.AnnotationMBe
anExporter        : Registering beans for JMX exposure on startup
2016-05-19 22:49:42.183  INFO 6452 --- [           main] o.s.c.support.DefaultLi
fecycleProcessor  : Starting beans in phase -2147482648
2016-05-19 22:49:42.183  INFO 6452 --- [           main] o.s.c.support.DefaultLi
fecycleProcessor  : Starting beans in phase 2147483647
2016-05-19 22:49:42.414  INFO 6452 --- [cTaskExecutor-1] o.s.a.r.c.CachingConnec
tionFactory       : Created new connection: SimpleConnection@4c9472e0 [delegate=
amqp://guest@172.20.122.9:5672/]
2016-05-19 22:49:42.819  INFO 6452 --- [           main] s.b.c.e.t.TomcatEmbedde
dServletContainer : Tomcat started on port(s): 8080 (http)
2016-05-19 22:49:42.819  INFO 6452 --- [           main] demo_rabbitmq.rabbit_de
mo.App            : Started App in 3.726 seconds (JVM running for 4.159)
Reply from server is: Hello TomasKloucek
```

Hope you found this useful. 
Next part is going to be last. I plan to test **publisher acks** functionality.

regards

Tomas